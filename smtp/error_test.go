package smtp

import "testing"

func TestSingleReply(t *testing.T) {
	got := smtpReply(replyCompleted, "OK").Error()
	expect := "250 OK\r\n"
	if got != expect {
		t.Errorf("got %#v but expected %#v\n", got, expect)
	}
}

func TestMultiReply(t *testing.T) {
	got := smtpReply(replyCompleted, "mydomain", "XTRA", "XMANY").Error()
	expect := "250-mydomain\r\n250-XTRA\r\n250 XMANY\r\n"
	if got != expect {
		t.Errorf("got %#v but expected %#v\n", got, expect)
	}
}
