package smtp

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"strings"
)

var (
	ErrInvalidMailbox    = errors.New("Invalid maibox")
	ErrTransactionDenied = errors.New("Transaction denied")
	ErrTransactionFailed = errors.New("Transaction failed")
	ErrNoStorage         = errors.New("Available storage space exceeded")
)

// Contains the mail envelope and its content.
type Mail struct {
	Remote   net.Addr      // Remote address of the sending SMTP client
	Hostname string        // Identification the remote SMTP client gave
	Reverse  string        // Reverse path (sender email address)
	Forward  []string      // Forward paths (receiver email addresses)
	Data     *bytes.Buffer // Mail data
}

// VerifyFunc defines a function that is used to verify if a mailbox (passed as
// "local@domain") exists.
//
// If a mailbox does not exist the function should return ErrInvalidMailbox. If
// processing fails for another reason the function should return an arbitrary
// error. This error will be logged by the SmtpClient handler. If the mailbox is
// valid, the function should return nil.
type VerifyFunc func(string) error

// StoreFunc defines a function that receives a Mail message and will store it
// into the mailboxes of the recipients. After this function returns the
// owership of the pointer is returned to the SmtpClient handler. If any of the
// fields need to be retained for later processing they need to be copied.
//
// If storage is denied for policy reasons the function should return
// ErrTransactionDenied. If storage fails because not enough disk space is
// available the function should return ErrNoStorage If the delivery fails
// permanently for another reason ErrTransactionFailed should be returned. If
// delivery fails for another reason, an arbitrary error should be returned.
// This error will be logged by the SmtpClient handler. If delivery succeeds,
// nil should be returned.
type StoreFunc func(*Mail) error

// LogFunc defines a function that logs messages. The first argument is defines
// the severity level of the log message, the second argument is a format string
// and the later arguments depend on the format string.
//
//  Severity levels:
//   Error    1
//   Warning  2
//   Notice   3
//   Info     4
//   Debug    5
type LogFunc func(uint, string, ...interface{})

const (
	logNone uint = iota
	logError
	logWarning
	logNotice
	logInfo
	logDebug
)

// Config contains all the configuration values needed by a client handler
type Config struct {
	Hostname string      // Hostname the server advertises in the SMTP greeting
	Verify   VerifyFunc  // See VerifyFunc
	Store    StoreFunc   // See StoreFunc
	Log      LogFunc     // See LogFunc
	Tls      *tls.Config // The server TLS configuration to use
}

// Client represents all the state and the handler of an SMTP client connection
type Client struct {
	conn net.Conn
	buf  *bufio.ReadWriter
	done bool
	helo bool
	tls  bool
	cfg  *Config
	mail *Mail
}

// NewClient creates a new SMTP client handler for a given connection. See the
// VerifyFunc and StoreFunc definitions for information on those handlers.
func NewClient(conn net.Conn, cfg *Config) *Client {
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
	return &Client{
		conn: conn,
		buf:  rw,
		done: false,
		helo: false,
		cfg:  cfg,
		tls:  false,
		mail: &Mail{
			Hostname: "",
			Reverse:  "",
			Forward:  make([]string, 0, 8),
			Data:     bytes.NewBuffer(nil)}}
}

func (clnt *Client) log(level uint, format string, args ...interface{}) {
	msg := fmt.Sprintf(format, args...)
	clnt.cfg.Log(level, "[%s] %s", clnt.conn.RemoteAddr(), msg)
}

// Forwards SMTP errors to the client, or logs/deals with other errors.
// NOTE: Keep in mind that this function can be called even when no SMTP error
// is expected.
func (clnt *Client) handleError(err error) bool {
	if e, ok := err.(*smtpError); ok {
		if e.trigger != nil {
			clnt.log(logError, "%s", e.trigger)
		}
		_, err = clnt.buf.Write(e.data)
		if err == nil {
			err = clnt.buf.Flush()
		}
	}
	if err != nil {
		clnt.log(logError, "%s (Unhandled)", err)
		return false
	}
	return true
}

// Handle handles a client connection from start to finish
func (clnt *Client) Handle() {
	defer func(c *Client) {
		// The connection object can get replaced by the STARTTLS handler so we
		// can't just defer clnt.conn.Close()
		// TODO: log connection close errors (doesn't matter for normal
		// connections, but as far as I know TLS has a close handshake that it
		// is probably nice to have error reporting on)
		clnt.conn.Close()
	}(clnt)
	defer clnt.log(logInfo, "Connection closed")
	clnt.mail.Remote = clnt.conn.RemoteAddr()
	clnt.log(logInfo, "New connection.")
	err := clnt.greeting()
	if err != nil && !clnt.handleError(err) {
		return
	}
	for !clnt.done {
		cmd, args, err := clnt.readCommand()
		if err != nil && !clnt.handleError(err) {
			return
		}
		err = clnt.handleCommand(cmd, args)
		if err != nil && !clnt.handleError(err) {
			return
		}
	}
}

// reply sends an SMTP reply consisting of a single line.
func (clnt *Client) reply(code uint, msg string) error {
	_, err := clnt.buf.WriteString(fmt.Sprintf("%d %s\r\n", code, msg))
	if err != nil {
		return err
	}
	return clnt.buf.Flush()
}

// Greeting sends the sever greeting to the client
func (clnt *Client) greeting() error {
	return clnt.reply(replyReady, fmt.Sprintf("%s ESMTP opmail", clnt.cfg.Hostname))
}

// Read a single command
func (clnt *Client) readCommand() (cmd string, args []string, err error) {
	// TODO: DJB suggests only caring about \n because many servers misbehave
	// and don't properly send \r\n. I don't know how accurate that is today.
	// This needs reviewing. http://cr.yp.to/smtp/request.html#request
	// FIXME: Some mitigation needs to be implemented to make sure that a
	// malicious client does not send a large (multi gigabyte) string never
	// ending in \n to make opmail consume all available memory.
	line, err := clnt.buf.ReadString('\n')
	if err != nil {
		return "", []string{}, err
	}
	// The following possibly trims "illegal" endings such as "\r\r\r\r\n" but
	// that does not have any real impact. Servers don't send that, and if they
	// do it doesn't matter.
	line = strings.TrimRight(line, "\r\n")
	parts := strings.Split(line, " ")
	if len(parts) > 1 {
		return parts[0], parts[1:], nil
	} else {
		return parts[0], []string{}, nil
	}
}

// handleCommand decides how to handle a specific command or passes it on to the
// appropriate handler.
func (clnt *Client) handleCommand(cmd string, args []string) error {
	clnt.log(logDebug, "got command %s with arguments %v", cmd, args)
	switch cmd {
	case "EHLO":
		return clnt.handleEhlo(args)
	case "HELO":
		return clnt.handleHelo(args)
	case "MAIL":
		return clnt.handleMail(args)
	case "RCPT":
		return clnt.handleRecipient(args)
	case "DATA":
		return clnt.handleData(args)
	case "RSET":
		return clnt.handleReset(args)
	case "NOOP":
		return clnt.handleNoop(args)
	case "QUIT":
		clnt.done = true
		return smtpReply(replyQuit, "OK")
	case "VRFY":
		return clnt.handleVerify(args)
	case "STARTTLS":
		return clnt.handleStartTls(args)
	default:
		return smtpReply(replyCommandError, "Command not recognized")
	}
}

// Reset returns the client into the default state, clearing all command related
// buffers
func (clnt *Client) reset() {
	clnt.mail.Reverse = ""
	clnt.mail.Forward = clnt.mail.Forward[0:0]
	clnt.mail.Data.Reset()
}

// handleReset resets the mail buffers
func (clnt *Client) handleReset(args []string) error {
	if len(args) != 0 {
		return smtpReply(replyArgumentError, "Excessive arguments")
	}
	clnt.reset()
	return smtpReply(replyCompleted, "OK")
}

// handleNoop does nothing
func (clnt *Client) handleNoop(args []string) error {
	if len(args) != 0 {
		return smtpReply(replyArgumentError, "Excessive arguments")
	}
	return smtpReply(replyCompleted, "OK")
}

// handleEhlo handles an EHLO command and sends the proper replies.
func (clnt *Client) handleEhlo(args []string) error {
	clnt.reset()
	if len(args) < 1 {
		return smtpReply(replyArgumentError, "Missing argument: EHLO domain|addr")
	}
	clnt.mail.Hostname = args[0]
	clnt.helo = true
	if clnt.cfg.Tls != nil && !clnt.tls {
		return smtpReply(replyCompleted, clnt.cfg.Hostname,
			"PIPELINING", "SIZE 1024000", "STARTTLS", "8BITMIME")
	} else {
		return smtpReply(replyCompleted, clnt.cfg.Hostname,
			"PIPELINING", "SIZE 1024000", "8BITMIME")
	}
}

func (clnt *Client) handleHelo(args []string) error {
	clnt.log(logWarning, "Client issued HELO instead of EHLO")
	clnt.reset()
	if len(args) < 1 {
		return smtpReply(replyArgumentError, "Missing argument: HELO domain")
	}
	clnt.mail.Hostname = args[0]
	clnt.helo = true
	return smtpReply(replyCompleted, clnt.cfg.Hostname)
}

func (clnt *Client) handleMail(args []string) error {
	// EHLO needs to happen first
	if !clnt.helo {
		return smtpReply(replyBadSequence, "Expected EHLO before MAIL")
	}
	// At least 1 argument
	if len(args) < 1 {
		return smtpReply(replyArgumentError, "Missing argument: MAIL FROM:<reverse-path>")
	}
	path := args[0]
	// Extra parameters (only SIZE for now)
	for _, arg := range args[1:] {
		if !strings.HasPrefix(arg, "SIZE=") {
			return smtpReply(replyParameterError, "Unknown parameter")
		}
	}
	if !strings.HasPrefix(path, "FROM:<") && !strings.HasSuffix(path, ">") {
		return smtpReply(replyArgumentError, "Incorrect argument: MAIL FROM:<reverse-path>")
	}
	path = path[6 : len(path)-1]
	if strings.Contains(path, ":") {
		path = strings.Split(path, ":")[1]
	}
	clnt.reset()
	clnt.mail.Reverse = path
	return smtpReply(replyCompleted, "OK")
}

func (clnt *Client) handleRecipient(args []string) error {
	// MAIL needs to happen first
	if len(clnt.mail.Reverse) == 0 {
		return smtpReply(replyBadSequence, "RCPT command only valid after initiating a transaction with MAIL")
	}
	// Only accept one argument
	if len(args) != 1 {
		return smtpReply(replyArgumentError, "Missing or excessive arguments: RCPT TO:<path>")
	}
	path := args[0]
	if !strings.HasPrefix(path, "TO:<") && strings.HasSuffix(path, ">") {
		return smtpReply(replyArgumentError, "Incorrect argument: RCPT TO:<path>")
	}
	path = path[4 : len(path)-1]
	err := clnt.cfg.Verify(path)
	switch {
	case err == ErrInvalidMailbox:
		return smtpReply(replyMailboxUnavailable, "Mailbox unavailable")
	case err != nil:
		return smtpReplyError(replyLocalError, err, "Internal error")
	}
	if strings.Contains(path, ":") {
		path = strings.Split(path, ":")[1]
	}
	clnt.mail.Forward = append(clnt.mail.Forward, path)
	return smtpReply(replyCompleted, "OK")
}

func (clnt *Client) handleData(args []string) error {
	if len(clnt.mail.Reverse) == 0 {
		return smtpReply(replyBadSequence, "No MAIL transaction in progress")
	}
	if len(clnt.mail.Forward) == 0 {
		return smtpReply(replyTransactionFailed, "No valid recipients")
	}
	if len(args) != 0 {
		if len(args) != 1 || !strings.HasPrefix(args[0], "BODY=") {
			return smtpReply(replyParameterError, "Unknown parameter")
		}
	}
	if err := clnt.reply(replyStartMail, "OK"); err != nil {
		return err
	}
	defer clnt.reset()
	for {
		rawline, err := clnt.buf.ReadString('\n')
		if err != nil {
			return err
		}
		line := strings.TrimRight(rawline, "\r\n")
		if line == "." {
			break
		}
		if len(line) > 1 && line[0] == '.' {
			rawline = rawline[1:]
		}
		clnt.mail.Data.WriteString(rawline)
	}
	err := clnt.cfg.Store(clnt.mail)
	switch {
	case err == ErrTransactionDenied:
		return smtpReply(replyMailboxUnavailable, "Transaction denied")
	case err == ErrTransactionFailed:
		return smtpReply(replyTransactionFailed, "Transaction failed")
	case err == ErrNoStorage:
		return smtpReply(replyNoStorage, "Available storage space exceeded")
	case err != nil:
		return smtpReplyError(replyLocalError, err, "Internal error")
	default:
		return smtpReply(replyCompleted, "OK")
	}
}

func (clnt *Client) handleVerify(args []string) error {
	if len(args) != 1 {
		return smtpReply(replyArgumentError, "Missing or excessive arguments: VRFY path")
	}
	if !strings.Contains(args[0], "@") {
		return smtpReply(replyArgumentError, "Invalid recipient argument")
	}
	// FIXME: proper parsing of argument
	err := clnt.cfg.Verify(args[0])
	switch {
	case err == ErrInvalidMailbox:
		return smtpReply(replyMailboxUnavailable, "Mailbox unavailable")
	case err != nil:
		return smtpReplyError(replyLocalError, err, "Internal error")
	default:
		return smtpReply(replyCompleted, "OK")
	}
}

func (clnt *Client) handleStartTls(args []string) error {
	switch {
	case clnt.cfg.Tls == nil:
		return smtpReply(replyNotImplemented, "STARTTLS not implemented")
	case len(args) != 0:
		return smtpReply(replyArgumentError, "No arguments allowed")
	case clnt.tls:
		return smtpReply(replyBadSequence, "TLS already started")
	}

	// Send the reply right here because after this function returns the
	// connection will be wrapped by TLS
	if err := clnt.reply(replyReady, "Ready to start TLS"); err != nil {
		return err
	}
	clnt.conn = tls.Server(clnt.conn, clnt.cfg.Tls)
	clnt.buf.Reader.Reset(clnt.conn)
	clnt.buf.Writer.Reset(clnt.conn)
	clnt.tls = true
	return nil
}
