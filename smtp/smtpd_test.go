package smtp

import (
	"bufio"
	"crypto/tls"
	"io"
	"net"
	"os"
	"strings"
	"testing"
)

var tlsCert = []byte("-----BEGIN CERTIFICATE-----\n" +
	"MIIB/DCCAWWgAwIBAgIJANWdThI8UxnvMA0GCSqGSIb3DQEBCwUAMBcxFTATBgNV\n" +
	"BAMMDHNtdHAudGVzdC5nbzAeFw0xNjA2MjEyMDI4MDRaFw00MzExMDcyMDI4MDRa\n" +
	"MBcxFTATBgNVBAMMDHNtdHAudGVzdC5nbzCBnzANBgkqhkiG9w0BAQEFAAOBjQAw\n" +
	"gYkCgYEAyVas8JWgW2UAbsP53Z41aWI9nry1mjUOcXwDb6xZASDr6Gzoa52zvZ2d\n" +
	"oVNSyNJKQR9ZavmR0ih/Dah/HuOvouZsFvV23xBU2T0JTKuoA8n6BKvcxDr5HYXz\n" +
	"5OIt1nlheiuyWptydhSj54Fx5d1k38FuVxJJZbHxx8JwtWw5z7cCAwEAAaNQME4w\n" +
	"HQYDVR0OBBYEFN909crgfyTtxopWj04gmBAPaZFBMB8GA1UdIwQYMBaAFN909crg\n" +
	"fyTtxopWj04gmBAPaZFBMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADgYEA\n" +
	"SEh5gj6fhcz5k8KCoEqxf7/IK9CiAsp7qvCLjo/rsNPukI2GmElvOg0X2znWQ8VQ\n" +
	"a874ekb2vcEO2ZhJfkXa+ZEEvA3W8LNjE8yhHSu+aErlOgZU5dwmuri33F/t5aqI\n" +
	"49liTEUd75hvxe7IthxaC5jN5nXb+HnuaQ3azRFnfPY=\n" +
	"-----END CERTIFICATE-----\n")

var tlsKey = []byte("-----BEGIN RSA PRIVATE KEY-----\n" +
	"MIICXwIBAAKBgQDJVqzwlaBbZQBuw/ndnjVpYj2evLWaNQ5xfANvrFkBIOvobOhr\n" +
	"nbO9nZ2hU1LI0kpBH1lq+ZHSKH8NqH8e46+i5mwW9XbfEFTZPQlMq6gDyfoEq9zE\n" +
	"OvkdhfPk4i3WeWF6K7Jam3J2FKPngXHl3WTfwW5XEkllsfHHwnC1bDnPtwIDAQAB\n" +
	"AoGBAMfEASlgPFASQcClIAi8zd/3Ao66TDFqDR+aXUpzJYhSIt9lF6KXQVyUw27q\n" +
	"3ou04soEkdLrNrzfQ1jI4wBCBuOFbN4DdrQYIaYshEt/mWA3M9hBNriozuEdeONG\n" +
	"G3i0vP3mYF5z5ZbUnZ3JxYFSXO8kBcvurcJ7Bog4EIKnpKLBAkEA9dEEeFV2iXBa\n" +
	"VIhBxiol3xRljCkVvEoJRgnQae0DGWz1JXXgy5BTZqv+JUwsGGvBQPWOQHxGeVtV\n" +
	"RQhAqJuMIQJBANGt87xqAuQu+EKRWW5cMePvqrfJR7wJANLJPy+F53LQd2u2xBXf\n" +
	"zUtTkOgy/Saavp4fB7Tapg/1VaY+PMWzINcCQQCm5F0jkeqjz2sUv1ngv9bsiT2U\n" +
	"loRSTPXU3m7oq+kRxx79avXPX1Kwf49SJVA+2FHh15XYBEgYtsDC2KKFlu3hAkEA\n" +
	"xfDdY0UArcDZhJ4KOvC1+GPMWeud2Jj3OgXZrFdQg0bhNcI3oh7AgxfEeC+KY+M5\n" +
	"h7PrZoPyuKX10RZ38HqFyQJBAMYJ+ozB/8xy9c/wuWPsMQn/hTz0vf51LfjYz5V3\n" +
	"qKFVFcuzE6G6GaydMTzwytZHx4R3GNqbechTfMLx9p3eIOg=\n" +
	"-----END RSA PRIVATE KEY-----\n")

func nilStore(m *Mail) error {
	return nil
}

func nilVerify(s string) error {
	return nil
}

func nilLog(l uint, m string, a ...interface{}) {
	return
}

func TestMain(m *testing.M) {
	// Create config
	cert, err := tls.X509KeyPair(tlsCert, tlsKey)
	if err != nil {
		panic(err)
	}
	cfg := &Config{
		Hostname: "smtp.test.go",
		Verify:   nilVerify,
		Store:    nilStore,
		Log:      nilLog,
		Tls: &tls.Config{
			Certificates: []tls.Certificate{cert}},
	}
	// Create listener
	fn, err := net.Listen("tcp", "127.0.0.1:52025")
	if err != nil {
		panic(err)
	}
	defer fn.Close()
	// Create server go routine
	go func(fn net.Listener, cfg *Config) {
		for {
			conn, err := fn.Accept()
			if err != nil {
				panic(err)
			}
			go NewClient(conn, cfg).Handle()
		}
	}(fn, cfg)
	// Run tests
	os.Exit(m.Run())
}

func createTest() (conn net.Conn, buf *bufio.ReadWriter) {
	conn, err := net.Dial("tcp", "127.0.0.1:52025")
	if err != nil {
		panic(err)
	}
	buf = bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
	return conn, buf
}

func testSend(t *testing.T, buf *bufio.ReadWriter, send string) {
	_, err := buf.WriteString(send)
	if err != nil {
		t.Errorf("Error sending: %s", err)
		t.FailNow()
		return
	}
	err = buf.Flush()
	if err != nil {
		t.Errorf("Error flushing: %s", err)
		t.FailNow()
		return
	}
}

func testReceive(t *testing.T, buf *bufio.ReadWriter, receive string) {
	str, err := buf.ReadString('\n')
	if err != nil {
		t.Errorf("Error reading string: %s", err)
		t.FailNow()
	}
	if !strings.HasPrefix(str, receive) {
		t.Errorf("Expected '%s' but got '%s'", strings.TrimRight(receive, "\r\n"),
			strings.TrimRight(str, "\r\n"))
		t.FailNow()
		return
	}
}

func testEOF(t *testing.T, buf *bufio.ReadWriter) {
	line, err := buf.ReadString('\n')
	if len(line) > 0 {
		t.Errorf("Received extra bytes at the end of the connection.")
		t.FailNow()
	}
	if err != io.EOF {
		t.Errorf("Didn't get the expected EOF")
		t.FailNow()
	}
}

func TestEhlo(t *testing.T) {
	conn, buf := createTest()
	defer conn.Close()

	testReceive(t, buf, "220 smtp.test.go")

	testSend(t, buf, "HELO\r\n")
	testReceive(t, buf, "501 ") // Argument error

	testSend(t, buf, "EHLO\r\n")
	testReceive(t, buf, "501 ") // Argument error

	testSend(t, buf, "HELO itsme\r\n")
	testReceive(t, buf, "250 smtp.test.go")

	testSend(t, buf, "EHLO itsme\r\n")
	testReceive(t, buf, "250-smtp.test.go")
	testReceive(t, buf, "250-PIPELINING")
	testReceive(t, buf, "250-SIZE")
	testReceive(t, buf, "250-STARTTLS")
	testReceive(t, buf, "250 8BITMIME")

	testSend(t, buf, "QUIT\r\n")
	testReceive(t, buf, "221 OK\r\n")
	testEOF(t, buf)
}

func TestNoHelo(t *testing.T) {
	conn, buf := createTest()
	defer conn.Close()
	testReceive(t, buf, "220 smtp.test.go")

	// VRFY
	testSend(t, buf, "VRFY\r\n")
	testReceive(t, buf, "501 ") // Argument error
	testSend(t, buf, "VRFY a@b extra\r\n")
	testReceive(t, buf, "501 ") // Argument error
	testSend(t, buf, "VRFY a@b\r\n")
	testReceive(t, buf, "250 ")

	// NOOP
	testSend(t, buf, "NOOP noop\r\n")
	testReceive(t, buf, "501 ") // Argument error
	testSend(t, buf, "NOOP\r\n")
	testReceive(t, buf, "250 ")

	// RSET
	testSend(t, buf, "RSET noop\r\n")
	testReceive(t, buf, "501 ") // Argument error
	testSend(t, buf, "RSET\r\n")
	testReceive(t, buf, "250 ") // Parameter error

	// MAIL
	testSend(t, buf, "MAIL\r\n")
	testReceive(t, buf, "503 ") // Bad sequence
	testSend(t, buf, "MAIL FROM:<me@you>\r\n")
	testReceive(t, buf, "503 ") // Bad sequence
	testSend(t, buf, "MAIL FROM:<me@you> PARAM=VALUE\r\n")
	testReceive(t, buf, "503 ") // Bad sequence

	// RCPT
	testSend(t, buf, "RCPT\r\n")
	testReceive(t, buf, "503 ") // Bad sequence
	testSend(t, buf, "RCPT TO:<me@you>\r\n")
	testReceive(t, buf, "503 ") // Bad sequence
	testSend(t, buf, "RCPT TO:<me@you> PARAM=VALUE\r\n")
	testReceive(t, buf, "503 ") // Bad sequence

	// DATA
	testSend(t, buf, "DATA\r\n")
	testReceive(t, buf, "503 ") // Bad sequence
	testSend(t, buf, "DATA PARAM=VALUE\r\n")
	testReceive(t, buf, "503 ") // Bad sequence

	// X-UNKNOWN
	testSend(t, buf, "X-UNKNOWN\r\n")
	testReceive(t, buf, "500 ") // Command error

	testSend(t, buf, "QUIT\r\n")
	testReceive(t, buf, "221 OK\r\n")
	testEOF(t, buf)
}

func TestStartTLS(t *testing.T) {
	conn, buf := createTest()
	defer func(conn *net.Conn) {
		(*conn).Close()
	}(&conn)
	testReceive(t, buf, "220 smtp.test.go")
	testSend(t, buf, "STARTTLS extra\r\n")
	testReceive(t, buf, "501 ") // Argument error
	testSend(t, buf, "STARTTLS\r\n")
	testReceive(t, buf, "220 Ready to start TLS")
	config := tls.Config{
		ServerName:         "smtp.test.go",
		InsecureSkipVerify: true}
	conn = tls.Client(conn, &config)
	buf.Reader.Reset(conn)
	buf.Writer.Reset(conn)
	testSend(t, buf, "STARTTLS\r\n")
	testReceive(t, buf, "503 ") // Bad sequence
	testSend(t, buf, "QUIT\r\n")
	testReceive(t, buf, "221 OK\r\n")
	testEOF(t, buf)
}

func TestTransaction(t *testing.T) {
	conn, buf := createTest()
	defer conn.Close()

	testReceive(t, buf, "220 smtp.test.go")

	testSend(t, buf, "HELO\r\n")
	testReceive(t, buf, "501 ") // Argument error

	testSend(t, buf, "EHLO\r\n")
	testReceive(t, buf, "501 ") // Argument error

	testSend(t, buf, "EHLO itsme\r\n")
	testReceive(t, buf, "250-smtp.test.go")
	testReceive(t, buf, "250-PIPELINING")
	testReceive(t, buf, "250-SIZE")
	testReceive(t, buf, "250-STARTTLS")
	testReceive(t, buf, "250 8BITMIME")

	testSend(t, buf, "MAIL FROM:<test@example.com>\r\n")
	testReceive(t, buf, "250 ")
	testSend(t, buf, "RCPT TO:<test@smtp.test.go>\r\n")
	testReceive(t, buf, "250 ")
	testSend(t, buf, "DATA\r\n")
	testReceive(t, buf, "354 ")
	testSend(t, buf, "body\r\n.\r\n")
	testReceive(t, buf, "250 ")

	testSend(t, buf, "MAIL FROM:<test@example.com>\r\n")
	testReceive(t, buf, "250 ")
	testSend(t, buf, "RCPT TO:<test@smtp.test.go>\r\n")
	testReceive(t, buf, "250 ")
	testSend(t, buf, "DATA EXTRA=INVALID\r\n")
	testReceive(t, buf, "504 ")

	testSend(t, buf, "MAIL FROM:<test@example.com>\r\n")
	testReceive(t, buf, "250 ")
	testSend(t, buf, "RCPT TO:<test@smtp.test.go>\r\n")
	testReceive(t, buf, "250 ")
	testSend(t, buf, "DATA BODY=8BITMIME\r\n")
	testReceive(t, buf, "354 ")
	testSend(t, buf, "body\r\n.\r\n")
	testReceive(t, buf, "250 ")

	testSend(t, buf, "QUIT\r\n")
	testReceive(t, buf, "221 OK\r\n")
	testEOF(t, buf)
}
