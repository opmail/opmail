package smtp

import (
	"bytes"
	"fmt"
)

const (
	replyHelp                   = 214
	replyReady                  = 220
	replyQuit                   = 221
	replyCompleted              = 250
	replyStartMail              = 354
	replyTempUnavailable        = 421
	replyMailboxTempUnavailable = 450
	replyLocalError             = 451
	replyNoStorage              = 452
	replyTlsError               = 454
	replyCommandError           = 500
	replyArgumentError          = 501
	replyNotImplemented         = 502
	replyBadSequence            = 503
	replyParameterError         = 504
	replyMailboxUnavailable     = 550
	replyTransactionFailed      = 554
)

// SmtpError represents an SMTP reply. It may also contain a successful reply,
// so it does not necessarily indicate an error condition. When this error
// reaches the main connection handler it will send out an SMTP reply and log
// the trigger.
type smtpError struct {
	code    uint
	trigger error
	data    []byte
}

func smtpReply(code uint, msgs ...interface{}) error {
	return smtpReplyError(code, nil, msgs...)
}

func smtpReplyError(code uint, trigger error, msgs ...interface{}) error {
	if len(msgs) == 0 {
		panic("SmtpReplyError needs at least 1 message argument")
	}
	buffer := bytes.NewBuffer(nil)
	for i, msg := range msgs {
		if i < len(msgs)-1 {
			fmt.Fprintf(buffer, "%d-%s\r\n", code, msg)
		} else {
			fmt.Fprintf(buffer, "%d %s\r\n", code, msg)
		}
	}
	return &smtpError{code: code, trigger: trigger, data: buffer.Bytes()}
}

func (e *smtpError) Error() string {
	return string(e.data)
}
