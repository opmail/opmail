// Really simple smtp server that just dumps mail in a directory.
//
//   Usage: opsmtpd [-config file]
//     -config string
//             Configuration file path (default "/etc/opmail/opmail.json")

package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"net"
	"os"

	"gitlab.com/opmail/opmail/smtp"
)

var cfgFile string // Command line parameter for config path

func init() {
	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, "Usage: opsmtpd [-config file]")
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.StringVar(&cfgFile, "config", "/etc/opmail/opmail.json", "Configuration file path")
}

func args() {
}

func main() {
	flag.Parse()
	loadConfig(cfgFile)
	initLogfile(cfg.Logfile, cfg.LogLevel)
	ln, err := net.Listen("tcp", cfg.Addr)
	if err != nil {
		fatalf(logError, "Fatal error opening listening socket: %s", err)
	}
	smtpCfg := &smtp.Config{
		Hostname: cfg.Hostname,
		Log:      logf,
		Verify:   verify,
		Store:    store,
	}
	if cfg.TlsCert != "" && cfg.TlsKey != "" {
		cert, err := tls.LoadX509KeyPair(cfg.TlsCert, cfg.TlsKey)
		if err != nil {
			fatalf(logError, "Fatal error loading TLS certificate: %s", err)
		}
		smtpCfg.Tls = &tls.Config{
			Certificates: []tls.Certificate{cert},
			MinVersion:   tls.VersionTLS12,
			CipherSuites: []uint16{tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384}}
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("error", err)
		}
		go smtp.NewClient(conn, smtpCfg).Handle()
	}
}
