package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

// JSON configuration file structure
var cfg struct {
	Aliases  map[string]string // Map mailbox to destination directory
	Addr     string            // Listen address
	LogLevel string            // none, error, warning, info, debug
	Logfile  string            // File to log to
	Hostname string            // Hostname to advertise
	TlsKey   string            // Key file
	TlsCert  string            // Cert file
}

func loadConfig(path string) {
	cfgData, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("Failed to open config file: %s", err)
	}
	json.Unmarshal(cfgData, &cfg)
	if cfg.Hostname == "" {
		var err error
		cfg.Hostname, err = os.Hostname()
		if err != nil {
			log.Fatalf("Failed to read hostname (/proc not mounted?): %s", err)
		}
	}
}
