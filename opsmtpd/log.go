package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

var logger *log.Logger
var maxLevel uint

const (
	logNone = iota
	logError
	logWarning
	logNotice
	logInfo
	logDebug
)

var logNames = []string{"NONE", "ERROR", "WARNING", "NOTICE ", "INFO", "DEBUG"}

func initLogfile(path, level string) {
	switch strings.ToLower(level) {
	case "none":
		maxLevel = logNone
	case "", "error":
		maxLevel = logError
	case "warning":
		maxLevel = logWarning
	case "notice":
		maxLevel = logNotice
	case "info":
		maxLevel = logInfo
	case "debug":
		maxLevel = logDebug
	default:
		logger.Fatalf("Unknown loglevel `%s`.", cfg.LogLevel)
	}

	switch path {
	case "stdout", "":
		logger = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	case "stderr":
		logger = log.New(os.Stderr, "", log.Ldate|log.Ltime)
	default:
		f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0640)
		if err != nil {
			log.Fatalln("Fatal error opening log file:", err)
		}
		logger = log.New(f, "", log.Ldate|log.Ltime)
	}
}

func logf(level uint, format string, args ...interface{}) {
	if level > maxLevel {
		return
	}
	msg := fmt.Sprintf(format, args...)
	if logger == nil {
		log.Printf("%.7s %s", logNames[level], msg)
	} else {
		logger.Printf("%.7s %s", logNames[level], msg)
	}
}

func fatalf(level uint, format string, args ...interface{}) {
	logf(level, format, args...)
	os.Exit(1)
}
