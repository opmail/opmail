package main

import (
	"bytes"
	"fmt"
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/opmail/opmail/smtp"
)

func verify(mailbox string) error {
	if _, ok := cfg.Aliases[mailbox]; !ok {
		return smtp.ErrInvalidMailbox
	}
	return nil
}

func store(mail *smtp.Mail) error {
	// FIXME: Store isn't a transaction. if storing for the second recipient
	// fails, it will return an error but the first mail will have been
	// delivered.
	reader := bytes.NewReader(mail.Data.Bytes())
	for _, mailbox := range mail.Forward {
		dir := cfg.Aliases[mailbox]
		fp := filepath.Join(dir, fmt.Sprintf("%d-%d", time.Now().Unix(), rand.Int63()))
		f, err := os.Create(fp)
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = fmt.Fprintf(f, "Return-Path: <%s>\r\n", mail.Reverse)
		if err != nil {
			return err
		}
		_, err = io.Copy(f, reader)
		if err != nil {
			return err
		}
		reader.Seek(0, 0)
	}
	return nil
}
